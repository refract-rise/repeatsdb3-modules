from setuptools import setup

with open("README.md", 'r') as f:
    long_description = f.read()

setup(
   name='repeatsdb3-annotation',
   version='1.0',
   description='Python library for dealing with RepeatsDB3 data',
   license="GNU AGPLv3",
   long_description=long_description,
   author='Lisanna Paladin',
   author_email='lisanna.paladin@gmail.com',
   packages=['repeatsdb3-annotation'],  #same as name
   mongo='172.21.2.89:27017'
)
