# calculations
import pandas
import numpy
from itertools import groupby

# plotting
from matplotlib import pyplot as plt
import seaborn as sns
from matplotlib.lines import Line2D
from matplotlib.colors import Normalize
import matplotlib.cm as cm
import matplotlib.ticker as ticker
@ticker.FuncFormatter
def major_formatter(x, pos):
    label = int(-x) if x < 0 else int(x)
    return label

import argparse

# configuration
import json
with open("modules/config.json") as json_data_file:
    config = json.load(json_data_file)
interval, maxval = 5, 60


def readdata(path):
    return pandas.read_csv(path + 'reviewed_yes.tab', sep='\t')


def selectby(columns, df):
    return [d for d in df[columns].to_numpy()
                    if all([isinstance(d[i], str) for i in range(len(columns))])]


def parserepeat(reptext, acc):
    repeats, lengths = [], []
    for r in reptext.split('REPEAT')[1:]:
        text = r.split(' /')
        start_end = text[0].strip(' ').strip(';').strip('<').replace('>', '')
        other = {t.split('=')[0]: t.split('="')[1].replace('"; ', '').strip('"') for t in text[1:]}
        other['start'] = int(start_end.split('..')[0]) if '..' in start_end else int(start_end)
        other['end'] = int(start_end.split('..')[1]) if '..' in start_end else int(start_end)
        repeats.append(other)
        length = other['end'] - other['start'] + 1
        lengths.append(length)
    return {'acc': acc, 'avg_len': numpy.mean(lengths), 'repeats': repeats}


def getlabels():
    # expected groups
    labels = [str(s)+'-'+str(s+10) for s in range(0, maxval+10, interval)]
    labels.reverse()
    labels[0] = str(maxval) + '+'
    return labels


def processdisorder(dataframe, also_not_disorder=False, order_PDB=False):
    def getarray(u):
        k = "derived-observed-th_90" if order_PDB else "prediction-disorder-th_50"
        if k in u:
            return set([i for r in u[k]['regions']
                        if r[1]-r[0] > 5 # filter out short repeat
                        for i in range(r[0], r[1]+1)])
        else:
            return set()
    dataframe_index = {d['acc']: d for d in dataframe}
    jsondata = json.load(open(config['paths']['absolute'] + config['paths']['mobidb-data'] + 'mobidb_human.json'))
    for u in jsondata:
        acc = u['acc']
        if acc in dataframe_index and 'prediction-disorder-th_50' in u:  # if not, entry in UniProt not SwissProt
            # check overlap between disorder and repeats
            referencearray = getarray(u)
            # filter out short overlaps
            filterrepeats = [r for r in dataframe_index[acc]['repeats']
                             if len(set(range(r['start'], r['end']+1)) & referencearray) >= (r['end']-r['start'])/4]
            refk = 'avg_len_order' if order_PDB else 'avg_len_disorder'
            dataframe_index[acc][refk] = numpy.mean([r['end'] - r['start'] for r in filterrepeats])
            dataframe_index[acc]['filtered_repeats'] = filterrepeats
            if also_not_disorder:
                filterrepeats2 = [r for r in dataframe_index[acc]['repeats']
                                 if len(set(range(r['start'], r['end'] + 1)) & referencearray) < (
                                             r['end'] - r['start']) / 4]  # filter out short overlaps
                refk2 = 'avg_len_disorder' if order_PDB else 'avg_len_order'
                dataframe_index[acc][refk2] = numpy.mean([r['end'] - r['start'] for r in filterrepeats2])
                dataframe_index[acc]['filtered_repeats'] = filterrepeats2
    return [dataframe_index[d] for d in dataframe_index] # remove indexes from dataframe


def getgroup(labels, avg):
    lbl = [l for l in labels
           if '+' not in l and int(l.split('-')[1]) >= avg > int(l.split('-')[0])]
    if not lbl:
        return labels[0]
    else:
        return lbl[0]


def uniprot_dataframe(human=True):
    dataframe = readdata(config['paths']['absolute'] + config['paths']['uniprot-data'])
    # Index([u'Entry', u'Entry name', u'Status', u'Protein names', u'Gene names',
    #        u'Organism', u'Length', u'Involvement in disease',
    #        u'Biotechnological use', u'Pharmaceutical use',
    #        u'Biotechnological use.1', u'Subunit structure [CC]', u'Interacts with',
    #        u'3D', u'Gene ontology (GO)', u'Subcellular location [CC]',
    #        u'Post-translational modification', u'Repeat', u'Domain [CC]'],
    #       dtype='object')
    if human:
        flt = dataframe['Organism'] == 'Homo sapiens (Human)'
        dataframe = dataframe[flt]
    repeats = selectby(['Repeat', 'Entry'], dataframe)
    repeats_parsed = [parserepeat(r[0], r[1]) for r in repeats]
    return dataframe, repeats_parsed


def add_to_dataset(usedkey, p, labels, data):
    if usedkey in p and not numpy.isnan(p[usedkey]):  # check if protein in dataset
        data.append({
            'acc': p['acc'],
            'avg_len': p[usedkey],
            'num_rep': len(p['filtered_repeats']),
            'group': getgroup(labels, p[usedkey])
        })
    return data


def uniprot_analysis(human=True):
    dataframe, repeats_parsed = uniprot_dataframe(human=human)
    # change avg_len_disorder to avg_len for the entire SwissProt distribution
    if human:
        repeats_parsed = processdisorder(repeats_parsed)
        usedkey = 'avg_len_disorder'
    else:
        # TODO: implement process disorder for SwissProt dataset
        usedkey = 'avg_len'
    # compile dataset
    labels, data = getlabels(), []
    for p in repeats_parsed:
        data = add_to_dataset(usedkey, p, labels, data)
    print('***', len(data), 'UniProt entries processed')
    return data, list(dataframe['Entry'])


def uniprot_analysis_split_order(human=True, order_PDB=False):
    dataframe, repeats_parsed = uniprot_dataframe(human=human)
    if human:
        repeats_parsed = processdisorder(repeats_parsed, also_not_disorder=True , order_PDB=order_PDB)
        # 'avg_len_disorder' and 'avg_len_order'
        labels, data_disorder, data_order = getlabels(), [], []
        for p in repeats_parsed:
            data_disorder = add_to_dataset('avg_len_disorder', p, labels, data_disorder)
            data_order = add_to_dataset('avg_len_order', p, labels, data_order)
        print('***', len(data_disorder)+len(data_order), 'UniProt entries processed')
        return data_disorder, data_order, list(dataframe['Entry'])


# def uniprot_analysis_split_order(human=True):
#     dataframe, repeats_parsed = uniprot_dataframe(human=human)
#     repeatsdbdata = repeatsdb_analysis(datasetfilter=list(dataframe['Entry']))
#     repeats_parsed = processdisorder(repeats_parsed, also_not_disorder=True, order_PDB=True)
#     for p in repeats_parsed:
#         print(p)
#         exit()


def repeatsdb_analysis(datasetfilter=None):

    jsondata = json.load(open(config['paths']['absolute'] + config['paths']['repeatsdb3-data'] + 'uniprots.json'))
    # dict_keys(['uniprot_id', 'uniprot_sequence', 'uniprot_name', 'interpro', 'pfam', 'pfam_consensus', 'pdb_chains',
    # 'repeatsdb_consensus_one', 'repeatsdb_consensus_majority', 'repeatsdb_consensus_all', 'exon_mapping'])
    if datasetfilter:
        jsondata = [u for u in jsondata if u['uniprot_id'] in datasetfilter]
    print('***', len(jsondata), 'RepeatsDB entries processed')
    # TODO: create dataset from this
    dataframe, labels = [], getlabels()
    for u in jsondata:
        avg_period = numpy.mean([c['period'] for c in u['repeatsdb_consensus_one']])
        # estimate number of units from period
        avg_number_units = numpy.mean([float(c['end']-c['start'])/c['period'] for c in u['repeatsdb_consensus_one']])
        dataframe.append({
            'acc': u['uniprot_id'],
            'avg_len': avg_period,
            'num_rep': avg_number_units,
            'group': getgroup(labels, avg_period)
        })
    return dataframe


def change_width(ax, new_value):
    for patch in ax.patches:
        current_width = patch.get_width()
        diff = current_width - new_value

        # we change the bar width
        patch.set_width(new_value)

        # we recenter the bar
        patch.set_x(patch.get_x() + diff * .5)


def plothistogram(leftdata, rightdata, ratio=False, human=True, title=None):
    # from : https://stackoverflow.com/questions/60969101/how-to-build-a-population-pyramid-with-python

    fig, ax = plt.subplots(figsize=(7,4))
    # dataframes to files
    basepath = config['paths']['absolute'] + config['paths']['output-datasets']
    if title:
        basepath += title + '_'
    pandas.DataFrame(data=leftdata).to_csv(basepath + 'repeat-definition-unstructured.csv')
    pandas.DataFrame(data=rightdata).to_csv(basepath + 'repeat-definition-structured.csv')
    # get labels
    labels = getlabels()
    # plot datasets
    countu, countr = [-len([u for u in leftdata if u['group'] == l]) for l in labels], \
                     [len([u for u in rightdata if u['group'] == l]) for l in labels]
    # map number repetitions to color values
    colorsu_vals, colorsr_vals = [numpy.mean([u['num_rep'] for u in leftdata if u['group'] == l]) for l in labels], \
                       [numpy.mean([u['num_rep'] for u in rightdata if u['group'] == l]) for l in labels]
    minm, maxm = min(colorsu_vals+colorsr_vals), max(colorsu_vals+colorsr_vals)
    norm = Normalize(vmin=minm, vmax=maxm, clip=True)
    # color map
    mapperu = cm.ScalarMappable(norm=norm, cmap=cm.YlGnBu)
    mapperr = cm.ScalarMappable(norm=norm, cmap=cm.YlOrRd)
    colorsu, colorsr = [mapperu.to_rgba(v) for v in colorsu_vals], [mapperr.to_rgba(v) for v in colorsr_vals]
    # plot colormap
    # Create the inset axes and use it for the colorbar.
    cax1 = fig.add_axes([0.91, 0.15, 0.05, 0.3])  # x, y
    cbar1 = plt.colorbar(mapperu, ax=ax, cax=cax1)  # x, y
    cbar1.ax.set_ylabel('# of repeat units\nin unstructured proteins') #rotation=270)
    cax2 = fig.add_axes([0.91, 0.52, 0.05, 0.3])
    cbar2 = plt.colorbar(mapperr, ax=ax, cax=cax2)
    cbar2.ax.set_ylabel('# of repeat units\nin structured proteins')
    # dataframe and histogram plotting
    df = pandas.DataFrame({'Lengths': labels,
                           # convert UniProts to negative numbers
                           'UniProt': countu,
                           'RepeatsDB': countr})
    if ratio:
        # transform to percentages
        df['UniProt'] = [-(d/sum(countu))*100 for d in countu]
        df['RepeatsDB'] = [d / sum(countr)*100 for d in countr]
    bar_plot = sns.barplot(x='UniProt', y='Lengths', data=df, ax=ax, palette=colorsu) # color="#A1C9F4")  #palette="ch:s=.25,rot=-.25")
    sns.barplot(x='RepeatsDB', y='Lengths', data=df, ax=ax, palette=colorsr) # color="#FFB482")  #palette="light:#5A9")
    bar_plot.set(
        xlabel="Repeated Swiss-Prot human proteins" if not ratio else "Proteins%",
        ylabel="Repeat length in aa")  #, title="What is a repeat?")
    # adjust negative xticks
    ax.xaxis.set_major_formatter(major_formatter)
    # legend
    # legend_elements = [Line2D([0], [0], color='#1E7EB7', lw=4, label='Unstructured'),
    #                    Line2D([0], [0], color='#D30F20', lw=4, label='Structured')]
    # ax.legend(handles=legend_elements)
    if title:
        name = title
    else:
        name = "what_is_a_repeat" if not ratio else "what_is_a_repeat_ratio"
        if human:
            name += "_human"
    print("Output in:", config['paths']['absolute'] + config['paths']['output-plots'])
    fig.savefig(config['paths']['absolute'] + config['paths']['output-plots'] + name + '.png')


if __name__ == "__main__":  # execute only if run as a script

    # def str2bool(v):
    #     if v.lower() in ('yes', 'true', 't', 'y', '1'):
    #         return True
    #     elif v.lower() in ('no', 'false', 'f', 'n', '0'):
    #         return False
    #     else:
    #         raise argparse.ArgumentTypeError('Boolean value expected.')
    #
    # def arg_parser():  # Parser implementation
    #     parser = argparse.ArgumentParser(prog='what_is_a_repeat.py',
    #                                      epilog="  Example 1: what_is_a_repeat.py --human",
    #                                      formatter_class=argparse.RawDescriptionHelpFormatter)
    #     parser.add_argument('--human', type=str2bool, default=True, help='Limit analysis to human dataset')
    #     return parser

    # parse and config arguments
    # args = arg_parser()

    hmn = True
    # first version of the plot, UniProt + RepeatsDB
    uniprotdata, extendedataset = uniprot_analysis(human=True if hmn else False)
    repeatsdbdata = repeatsdb_analysis(datasetfilter=extendedataset)
    plothistogram(uniprotdata, repeatsdbdata, human=hmn)
    # second version
    # Disorder / Non disorder plot
    uniprotdisorder, uniprotnondisorder, df = uniprot_analysis_split_order(human=True if hmn else False)
    plothistogram(uniprotdisorder, uniprotnondisorder, human=hmn, title="what_is_a_repeat_human_disorder")
    # Order / Non order plot
    uniprotdisorder, uniprotnondisorder, df = uniprot_analysis_split_order(human=True if hmn else False, order_PDB=True)
    plothistogram(uniprotdisorder, uniprotnondisorder, human=hmn, title="what_is_a_repeat_human_order")
