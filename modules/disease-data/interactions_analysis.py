import argparse
import pandas
import numpy
import json
import subprocess
import logging
from sklearn.metrics import classification_report

# configuration
import json
with open("modules/config.json") as json_data_file:
    config = json.load(json_data_file)
intactdatafolder = config['paths']['absolute'] + config['paths']['intact-data']
repeatsdatafolder = config['paths']['absolute'] + config['paths']['repeatsdb3-data']


def reducefile():

    content = pandas.read_csv(intactdatafolder + 'intact.txt', sep='\t')
    columns = ['#ID(s) interactor A', 'ID(s) interactor B',
                  'Feature(s) interactor A', 'Feature(s) interactor B']
    print(len(content), 'total interactions')
    df = [d for d in content[columns].to_numpy() if 'binding' in d[2] or 'binding' in d[3]]
    print(len(df), 'interactions annotated with region')
    fl = open(intactdatafolder+'intact_reduced.txt', 'w+')
    fl.write('\t'.join(columns)+'\n')
    for row in df:
        fl.write('\t'.join(row))
        fl.write('\n')


def report():

    def parselocation(loc):
        resrange = []
        try:
            key, val = loc.split(':')
            # TODO: deal with n-nand c-c notations
            if '?' not in val and 'n' not in val and 'c' not in val:
                for value in val.split(','):
                    if '..' in value:
                        resrange += range(int(value.split('-')[0].split('..')[1]) - 1, int(value.split('-')[1].split('..')[0]))
                    else:
                        resrange += range(int(value.split('-')[0])-1, int(value.split('-')[1].split('(')[0]))
        except:
            return resrange
        return resrange

    def preload():

        def splitid(key):
            return [d.split(':')[1].split('-')[0] for d in intact[key] if d != '-']

        intact = pandas.read_csv(intactdatafolder + 'intact_reduced.txt', sep='\t')
        repeatsdb = json.loads(open(repeatsdatafolder + 'uniprots.json').read())
        intact_ids = set(splitid('#ID(s) interactor A')).union(
            set(splitid('ID(s) interactor B'))
        )
        repeatsdb_ids = set([d['uniprot_id'] for d in repeatsdb])
        dataset = repeatsdb_ids & intact_ids
        print(len(dataset), 'dataset length')
        fastafl = '\n'.join(['>' + d['uniprot_id'] + '\n' + d['uniprot_sequence']
                             for d in repeatsdb if d['uniprot_id'] in dataset])
        with open(repeatsdatafolder + 'intact_dataset.mfas', 'w+') as fl:
            fl.write(fastafl)
            fl.close()
        # print(len(dataset))
        ## process info in the dataset ##
        # CD-hit
        # TODO: to check compatibility with environment
        cmd = 'cdhit -i ' + repeatsdatafolder + 'intact_dataset.mfas -o ' + \
              repeatsdatafolder + 'intact_dataset_cdhit_100.fa -c 0.7'
        with subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE, encoding="utf-8") as proc:
            stdout = proc.stdout.readlines()
            stderr = proc.stderr.readlines()
        if stderr:
            logging.error(f'{stderr}. Check again...')
            stdout = []

        reduced_dataset = [ln.strip('>') for ln in
                           open(repeatsdatafolder + 'intact_dataset_cdhit.fa').read().split('\n') if '>' in ln]
        intact = pandas.read_csv(intactdatafolder + 'intact_reduced.txt', sep='\t')
        finaldic = {u: [] for u in reduced_dataset}
        for index, row in intact.iterrows():
            a = row['#ID(s) interactor A'].split(':')[1].split('-')[0]
            b = row['ID(s) interactor B'].split(':')[1].split('-')[0] if row['ID(s) interactor B'] != '-' else None
            if a in reduced_dataset:
                if 'binding' in row['Feature(s) interactor A']:
                    finaldic[a].append({
                        'interactor': b,
                        'location': row['Feature(s) interactor A']
                    })
            if b and b in reduced_dataset:
                if 'binding' in row['Feature(s) interactor B']:
                    finaldic[b].append({
                        'interactor': a,
                        'location': row['Feature(s) interactor B']
                    })
        repdic = {u['uniprot_id']: u for u in json.loads(open(repeatsdatafolder + 'uniprots.json').read())}
        reducedfinaldic = {
            u: {
                'interactions': finaldic[u],
                'repeats': repdic[u]
            } for u in finaldic if finaldic[u]}
        open(repeatsdatafolder + 'intact_dataset.json', 'w+').write(json.dumps(reducedfinaldic))

        dataset = json.loads(open(repeatsdatafolder + 'intact_dataset.json').read())
        interestingtypes = set(['sufficient binding region', 'necessary binding region',
                                'direct binding region', 'binding-associated region'])
        filtered_dataset = [d for d in dataset if interestingtypes & set(
            [loc.split(':')[0] for inte in dataset[d]['interactions'] for loc in inte['location'].split('|')]
        )]
        for protein in filtered_dataset:
            prange = range(len(dataset[protein]['repeats']['uniprot_sequence']))
            # print(protein, len(prange))
            repeat_array = [1 if index in set([ii
                                               for reg in dataset[protein]['repeats']['repeatsdb_consensus_one'] for ii
                                               in range(
                    reg['start'] - 1, reg['end'])]) else 0
                            for index in prange]
            interaction_array = [1 if index in set(
                [ii for inte in dataset[protein]['interactions'] for loc in inte['location'].split('|')
                 for ii in parselocation(loc)]
            ) else 0 for index in prange]
            dataset[protein]['repeat_array'] = repeat_array
            dataset[protein]['interaction_array'] = interaction_array
        open(repeatsdatafolder + 'intact_dataset_arrays.json', 'w+').write(json.dumps(dataset))

    preload() # comment to run only statistical analysis on precalculate files
    dataset = json.loads(open(repeatsdatafolder + 'intact_dataset_arrays.json').read())
    overlaprepeat, overlapinteraction = [], []
    correct_flat, predicted_flat = [], []
    for protein in dataset:
        if 'repeat_array' in dataset[protein] and 'interaction_array' in dataset[protein]:
            repeat_array, interaction_array = dataset[protein]['repeat_array'], dataset[protein]['interaction_array']
            prange = range(len(dataset[protein]['repeats']['uniprot_sequence']))
            # overlap with respect to repeats
            sumoverlap = float(sum([1 for i in prange if repeat_array[i] == 1 and interaction_array[i] == 1]))
            overlaprepeat.append(sumoverlap/sum(repeat_array))
            correct_flat += repeat_array
            if sum(interaction_array):
                overlapinteraction.append(sumoverlap / sum(interaction_array))
                if 1 > (sumoverlap / sum(interaction_array)) > 0.9:
                    print(protein)
            else:
                overlapinteraction.append(0)
            predicted_flat += interaction_array
            # print(classification_report(repeat_array, interaction_array))
    print(classification_report(correct_flat, predicted_flat))
    print(numpy.mean(overlaprepeat), 'repeat')
    print(numpy.mean(overlapinteraction), 'interaction')


if __name__ == "__main__":  # execute only if run as a script

    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    def arg_parser():  # Parser implementation
        parser = argparse.ArgumentParser(prog='interactions_analysis.py',
                                         epilog="  Example 1: interactions_analysis.py --report",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('--reducefile', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Write basic report. If no other option is selected, this will run automatically.")
        parser.add_argument('--report', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Write basic report. If no other option is selected, this will run automatically.")
        return parser.parse_args()

    # parse and config arguments
    args = arg_parser()

    if args.reducefile:
        reducefile()
    if args.report:
        report()