import pandas
import numpy
import argparse
from scipy import stats
import matplotlib.pyplot as plt
import seaborn as sns

# configuration
import json
with open("modules/config.json") as json_data_file:
    config = json.load(json_data_file)


def selectby(columns, df):

    return [d for d in df[columns].to_numpy()
                    if all([isinstance(d[i], str) for i in range(len(columns))])]


def readdata(path):

    return pandas.read_csv(path + 'reviewed_yes.tab', sep='\t')


def plothist(distribution):

    # Plot histogram #
    fig, ax = plt.subplots(figsize=(6, 4))
    bins, xlimmax = 100, 20
    plt.hist(distribution, density=True, bins=bins)  # change density to true, because KDE uses density
    # Plot KDE
    kde = stats.gaussian_kde(distribution)
    space = numpy.linspace(0, xlimmax, bins)
    plt.plot(space, kde(space))
    # Labels #
    ax.set_xlabel("Number of interactors")
    ax.set_xlim(0, xlimmax)
    # Relabel the axis as "Frequency"
    ax.set_ylabel("Frequency")
    # Overall #
    ax.set_title("Number of interactors in Uniprot dataset")
    # Remove ticks and spines
    ax.tick_params(left=False, bottom=False)
    for ax, spine in ax.spines.items():
        spine.set_visible(False)
    plt.show()


def testdifference(parameter, name, separator=';', testtype='ttest'):

    dataset = readdata(config['paths']['absolute'] + config['paths']['uniprot-data'])
    distribution = [len(d[0].split(separator)) if isinstance(d[0], str) else 0
                    for d in dataset[[parameter]].to_numpy()]
    repeat_distribution = [len(d[0].split(separator)) if isinstance(d[0], str) else 0
                           for d in dataset[[parameter, 'Repeat']].to_numpy()
                           if isinstance(d[1], str)]
    non_repeat_distribution = [len(d[0].split(separator)) if isinstance(d[0], str) else 0
                           for d in dataset[[parameter, 'Repeat']].to_numpy()
                           if not isinstance(d[1], str)]
    # print(dataset[parameter])
    # plothist(distribution)
    # report #
    outfile = open(config['paths']['absolute'] + config['paths']['output'] + 'repeat_{}_report.txt'.format
    (name), 'w+')
    outfile.write("Mean number of {} in background: ".format(name) + str(numpy.mean(distribution)) + '\n')
    outfile.write("Mean number of {} in repeats: ".format(name) + str(numpy.mean(repeat_distribution)) + '\n')
    if testtype == 'ttest':
        # plot
        plt.figure(figsize=(10, 7), dpi=300)
        kwargs = dict(hist_kws={'alpha': .6}, kde_kws={'linewidth': 2})
        sns.distplot(repeat_distribution, bins=1000, color="dodgerblue",
                     label="Repeat containing proteins in SwissProt, mean: "+str(round(numpy.mean(repeat_distribution), 2)), **kwargs)
        sns.distplot(non_repeat_distribution, bins=1000, color="orange",
                     label="Proteins in SwissProt without repeats, mean: "+str(round(numpy.mean(non_repeat_distribution), 2)), **kwargs)
        plt.xlim(0, 30)
        extraticks = [13, 18, 21]
        plt.xticks(list(plt.xticks()[0]) + extraticks)
        plt.ylabel("Density")
        plt.xlabel("Number of interactors")
        plt.legend()
        plt.savefig(config['paths']['absolute'] + config['paths']['output'] + 'repeat_{}_distributions.png'.format(name))
        # statistical test
        sttest = stats.ttest_ind(repeat_distribution, distribution)
        # for one tailed, divide the p-value by 2
        pvalue = sttest.pvalue/2
        outfile.write("Test difference between repeats and background through Student's t test, p-value: " + str(pvalue))
    elif testtype == 'fisher':
        # create contingency table
        #               repeat    non repeat
        # disease
        # non disease
        disease = [sum([1 for e in repeat_distribution if e > 0]), sum([1 for e in distribution if e > 0])]
        no_disease = [sum([1 for e in repeat_distribution if e == 0]), sum([1 for e in distribution if e == 0])]
        oddsratio, pvalue = stats.fisher_exact([disease, no_disease])
        outfile.write("Test difference between repeats and background through Fisher extact test, p-value: " + str(pvalue))


def printreport():

    def roundmath(subset, background):
        return str(round(float(len(subset))/len(background)*100, 2))

    dataframe = readdata(config['paths']['absolute'] + config['paths']['uniprot-data'])
    # Index([u'Entry', u'Entry name', u'Status', u'Protein names', u'Gene names',
    #        u'Organism', u'Length', u'Involvement in disease',
    #        u'Biotechnological use', u'Pharmaceutical use',
    #        u'Biotechnological use.1', u'Subunit structure [CC]', u'Interacts with',
    #        u'3D', u'Gene ontology (GO)', u'Subcellular location [CC]',
    #        u'Post-translational modification', u'Repeat', u'Domain [CC]'],
    #       dtype='object')

    # Iterating over multiple columns - same data type
    # result = [f(row[0], ..., row[n]) for row in df[['col1', ...,'coln']].to_numpy()]
    repeats = selectby(['Repeat'], dataframe)
    diseases = selectby(['Involvement in disease'], dataframe)
    repeats_diseases = selectby(['Repeat', 'Involvement in disease'], dataframe)
    pdbs = selectby(['3D'], dataframe)
    pdbs_diseases = selectby(['3D', 'Involvement in disease'], dataframe)
    pdbs_repeats = selectby(['3D', 'Repeat'], dataframe)
    pdbs_repeats_diseases = selectby(['3D', 'Involvement in disease', 'Repeat'], dataframe)

    # percentages
    outfile = open(config['paths']['absolute'] + config['paths']['output'] + 'repeat_disease_overlap_report.txt', 'w+')
    outfile.write(roundmath(diseases, dataframe) + ' % disease in dataset\n')
    outfile.write(roundmath(repeats_diseases, repeats) + ' % disease in repeats\n')
    outfile.write(roundmath(pdbs_diseases, pdbs) + ' % disease in pdbs\n')
    outfile.write(roundmath(pdbs_repeats_diseases, pdbs_repeats) + ' % disease in pdbs with repeats\n')


if __name__ == "__main__":  # execute only if run as a script

    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    def arg_parser():  # Parser implementation
        parser = argparse.ArgumentParser(prog='data-repeats-uniprot-pdb.py',
                                         epilog="  Example 1: data_repeats_pdb.py 1gxrA",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('--report', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Write basic report. If no other option is selected, this will run automatically.")
        parser.add_argument('--interactions', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Process interaction data.")
        parser.add_argument('--diseases', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Process disease data.")
        parser.add_argument('--ptms', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Process PTMs data.")
        parser.add_argument('--modifications', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Process modified residues data.")
        return parser.parse_args()


    # parse and config arguments
    args = arg_parser()

    if args.report:
        printreport()
    else:
        if 'interactions' not in args and 'diseases' not in args:
            printreport()
    if args.interactions:
        testdifference('Interacts with', 'interactions', testtype='ttest')
    if args.diseases:
        testdifference('Involvement in disease', 'diseases', testtype='fisher')
    if args.ptms:
        testdifference('Post-translational modification', 'ptms', testtype='ttest')
    if args.modifications:
        testdifference('Modified residue', 'modifications', testtype='ttest')



