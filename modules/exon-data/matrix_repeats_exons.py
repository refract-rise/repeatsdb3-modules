import os
import math
import json
import numpy as np
import seaborn as sns
import string

from matplotlib.lines import Line2D
from matplotlib.patches import Polygon
import matplotlib.pyplot as plt
from collections.abc import Iterable
from itertools import zip_longest


def int2roman(number):
    numerals = {1: "I", 4: "IV", 5: "V", 9: "IX", 10: "X", 40: "XL",
                50: "L", 90: "XC", 100: "C", 400: "CD", 500: "D", 900: "CM", 1000: "M"}
    result = ""
    for value, numeral in sorted(numerals.items(), reverse=True):
        while number >= value:
            result += numeral
            number -= value
    return result


def annotate_matrix(regs, m, ax, triu=False, fontsize=8):  # change here to regulate text size in cells
    for i, slice_y in enumerate(regs):
        for j, slice_x in enumerate(regs):
            if (triu is False and i > j) or (triu is True and i < j):
                x, y = slice_x.mean(), slice_y.mean()
                value = abs(m.T[int(x), int(y)])
                if not np.isnan(value):
                    if value < 0.5:
                        text_color = (0.0, 0.0, 0.0)
                    else:
                        text_color = (1.0, 1.0, 1.0)
                    ax.text(x, y, "{:.2f}".format(abs(value)),
                            ha="center", va="center", color=text_color, size=fontsize)


def merge_matrices(upper, lower, n, unset_diag=True):
    # transpose both created matrices
    # upper.transpose()
    # lower.transpose()

    # initialize new matrix of shape (lseq, lseq) with nan
    cmat = np.full((n, n), np.nan)

    # get indexes for triangular upper and lower portion of a matrix of shape (lseq, lseq)
    triu = np.triu_indices(n)
    tril = np.tril_indices(n)

    # fill upper portion of new matrix with upper portion of exonx matrix
    cmat[triu] = upper[triu]
    # fill lower portion of new matrix with lower portion of repeat units matrix
    cmat[tril] = lower[tril]
    if unset_diag is True:
        cmat[np.diag_indices(n, ndim=2)] = np.nan

    return cmat


def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)


def draw_rbox(ax, regions, n, facecolor, label, ylim=None, labelsize=8):
    ax.set_ylim(n, 0)
    ax.set_xlim(0.1, 0.9)
    ax.tick_params(axis='both', which='major', length=0)
    ax.yaxis.tick_right()
    ax.xaxis.tick_top()
    # ax.axes.get_xaxis().set_visible(False)
    make_patch_spines_invisible(ax)

    for reg in regions:
        start, end = reg
        ax.add_patch(Polygon([(0, start), (0, end), (1, end), (1, start)],
                             edgecolor='w', lw=2, facecolor=facecolor))

    yticks = np.unique(np.concatenate(regions))
    ax.set_yticks(yticks)
    ax.set_yticklabels(yticks, fontdict={"fontsize": labelsize})
    ax.set_xticks([0.5])
    ax.set_xticklabels([label], rotation=270)
    # plt.text(0, 0, label, rotation=270, horizontalalignment="center", verticalalignment="bottom")
    if ylim is not None:
        ax.set_ylim(*ylim[::-1])


def draw_bbox(ax, regions, n, facecolor, label, xlim=None, labelsize=8):
    ax.set_xlim(0, n)
    ax.set_ylim(0.1, 0.9)
    ax.tick_params(axis='both', which='major', length=0)
    # ax.axes.get_yaxis().set_visible(False)
    make_patch_spines_invisible(ax)

    for reg in regions:
        start, end = reg
        ax.add_patch(Polygon([(start, 0), (end, 0), (end, 1), (start, 1)], edgecolor='w', lw=2,
                             facecolor=facecolor))

    xticks = np.unique(np.concatenate(regions))
    ax.set_xticks(xticks)
    ax.set_xticklabels(xticks, rotation=90, fontdict={"fontsize": labelsize})
    ax.set_yticks([0.5])
    ax.set_yticklabels([label])
    if xlim is not None:
        ax.set_xlim(*xlim)


def draw_lines(ax, region, n, color, w, triu):
    start, end = region
    ystart = n if triu is False else 0
    xstart = 0 if triu is False else n
    # vertical lines
    ax.add_line(Line2D([end], (ystart, end), color=color, lw=w))
    ax.add_line(Line2D([start], (ystart, start), color=color, lw=w))
    # horizontal lines
    ax.add_line(Line2D((xstart, end), [end], color=color, lw=w))
    ax.add_line(Line2D((xstart, start), [start], color=color, lw=w))


def draw_projections(ax, region, n, color, alpha, triu):
    start, end = region
    if triu is True:
        points = [(n, start), (end, start), (end, 0), (start, 0),
                  (start, start), (end, end), (n, end)]
    else:
        points = [(start, n), (end, n), (end, end), (start, start),
                  (0, start), (0, end), (start, end)]

    ax.add_patch(Polygon(points, alpha=alpha, facecolor=color))


def draw_secondary_ticks_top(ax, ticks, ticks_n, fontsize=8):
    ax2 = ax.twiny()
    ax2.set_xlim(ax.get_xlim())
    ax2.set_ylim(ax.get_ylim())
    valid_strings = {"boundaries", "residues"}
    if isinstance(ticks, str):
        assert ticks in valid_strings, "Expected {} for 'secondary_ticks_top' arg; got {}".format(valid_strings, ticks)

        if ticks == "boundaries":
            ax2.set_xticks(ticks_n)
            ax2.set_xticklabels(ticks_n, rotation=90)
            ax2.tick_params(axis='x', which='major', direction='out', length=20, color='grey', labelsize=fontsize)
        elif ticks == "residues":
            ax2.tick_params(axis='x', which='major', pad=20, labelrotation=90, color='w', labelsize=fontsize)
    elif isinstance(ticks, Iterable):
        ax2.spines['top'].set_position(('axes', 1.1))
        ax2.set_xticks(ticks)
        ax2.set_xticklabels(ticks, rotation=90)
        ax2.tick_params(axis='x', which='major', length=0, labelsize=fontsize)
    make_patch_spines_invisible(ax2)


def draw_secondary_ticks_left(ax, ticks, ticks_n, fontsize=8):
    ax2 = ax.twinx()
    ax2.set_xlim(ax.get_xlim())
    ax2.set_ylim(ax.get_ylim())
    ax2.yaxis.tick_left()
    valid_strings = {"boundaries", "residues"}
    if isinstance(ticks, str):
        if isinstance(ticks, str):
            assert ticks in valid_strings, "Expected {} for 'secondary_ticks_left' arg; got {}".format(valid_strings,
                                                                                                       ticks)

        if ticks == "boundaries":
            ax2.set_yticks(ticks_n)
            ax2.set_yticklabels(ticks_n, rotation=0)
            ax2.tick_params(axis='y', which='major', direction='out', length=20, color='grey', labelsize=fontsize)
        elif ticks == "residues":
            ax2.tick_params(axis='y', which='major', pad=20, color='w', labelsize=fontsize)
        else:
            raise ValueError("Invalid 'secondary_ticks_left' selector")
    elif isinstance(ticks, Iterable):
        ax2.spines['left'].set_position(('axes', -0.1))
        ax2.set_yticks(ticks)
        ax2.set_yticklabels(ticks)
        ax2.tick_params(axis='y', which='major', length=0, labelsize=fontsize)
    make_patch_spines_invisible(ax2)


def uniform_matrix_from_regions(regions, n, value):
    mat = np.full([n, n], np.nan)

    for slice_x in regions:
        for slice_y in regions:
            mat[slice(*slice_x), slice(*slice_y)] = value
    return mat


def distance_matrix_from_regions(regs, n, dist, scale=None, erase_diag=True):
    mat = np.full([n, n], np.nan)

    for i, (row, x_slice) in enumerate(zip(dist, regs)):
        for j, (val, y_slice) in enumerate(zip(row, regs)):
            if erase_diag is True and i == j:
                val = np.nan
            else:
                if scale is not None:
                    rmin, rmax = scale if scale[1] > 0 else map(abs, scale[::-1])
                    val = val * (rmax - rmin) + rmin
                    val *= 1 if scale[1] > 0 else -1

            mat[slice(*x_slice), slice(*y_slice)] = val

    return mat


def adjust_regions(exons, gap=2):
    new_r = []
    prev_r = None
    n_exons = len(exons)
    for i, r in enumerate(exons, 1):
        if prev_r is not None:
            if 0 < (r[0] - prev_r[1]) <= gap:
                prev_r[1] = r[0]

            new_r.append(prev_r.tolist())

            if i == n_exons:
                new_r.append(r.tolist())

        prev_r = r

    return np.array(new_r)

def draw_matrix(cmat, bottom_regs, top_regs, v=(-1, 1), bboxes=None, rboxes=None,
                bboxes_labels=None, rboxes_labels=None,
                bottom_projections=None, top_projections=None, figsize=(5, 5), diagonal=True,
                secondaryticks_top=None, secondaryticks_left=None, square=True, cmap_name='bwr',
                edgecolor='w', edgewidth=2, bgrcolor='whitesmoke', outname='unitMatrix.png', clip=False):

    # parse arguments
    bboxes = bboxes if bboxes is not None else []
    rboxes = rboxes if rboxes is not None else []
    bboxes_labels = bboxes_labels if bboxes_labels is not None else []
    rboxes_labels = rboxes_labels if rboxes_labels is not None else []
    bottom_projections = bottom_projections if bottom_projections is not None else []
    top_projections = top_projections if top_projections is not None else []

    # set figure size parameters
    nbottoms = len(bboxes)
    nrights = len(rboxes)

    rows = 2 + nbottoms
    cols = 1 + nrights

    hratios = [.05, .9] + [0.01] * nbottoms
    wratios = [.9] + [0.01] * nrights

    grid_kws = {"height_ratios": hratios, "width_ratios": wratios, "hspace": 0.4}

    if square is True:
        bsize = figsize[0] * 0.04
        fwidth = figsize[0] + bsize * (nrights - nbottoms)
        fheight = figsize[0] + bsize * 1.5 + bsize * (nbottoms - nrights)
    else:
        fwidth, fheight = figsize

    all_regs = np.concatenate([top_regs, bottom_regs])

    if clip is True:
        start_res = np.min(all_regs)
        end_res = np.max(all_regs)
        maxlen = end_res
    else:
        start_res = 1
        end_res = cmat.shape[0]
        maxlen = cmat.shape[0]

    # maxlen = cmat.shape[0] if clip is False else end_res

    fig = plt.figure(constrained_layout=True, figsize=(fwidth, fheight))
    gs = fig.add_gridspec(rows, cols, **grid_kws)

    cbar_ax = fig.add_subplot(gs[0, 0])
    ax = fig.add_subplot(gs[1, 0])

    minreg = min(np.diff(all_regs))
    # meanreg = np.mean(np.diff(all_regs))
    afs = (minreg * fwidth) * 2 / (maxlen / (3 ** math.log10(maxlen)))  # change here to regulate label size
    tfs = afs * 1.5

    # draw bottom boxes
    for i, (bbox, bbox_label) in enumerate(zip_longest(bboxes, bboxes_labels, fillvalue=''), 2):
        bax = fig.add_subplot(gs[i, 0])
        draw_bbox(bax, bbox, maxlen, plt.get_cmap(cmap_name)(0.75), bbox_label,
                  xlim=(start_res, end_res), labelsize=tfs)

    # draw right boxes
    for i, (rbox, rbox_label) in enumerate(zip_longest(rboxes, rboxes_labels, fillvalue=''), 1):
        rax = fig.add_subplot(gs[1, i])
        draw_rbox(rax, rbox, maxlen, plt.get_cmap(cmap_name)(0.25), rbox_label,
                  ylim=(start_res, end_res), labelsize=tfs)

    # draw matrix
    sns.heatmap(cmat, vmin=v[0], vmax=v[1], cmap=cmap_name, cbar_ax=cbar_ax,
                ax=ax, cbar_kws={'orientation': 'horizontal'})

    ax.set_xlim(start_res, end_res)
    ax.set_ylim(end_res, start_res)

    # annotate matrix
    annotate_matrix(bottom_regs, cmat, ax, fontsize=afs)
    annotate_matrix(top_regs, cmat, ax, triu=True, fontsize=afs)

    # set nan values white
    ax.set_facecolor(bgrcolor)

    # draw bottom edges
    for br in bottom_regs:
        draw_lines(ax, br, maxlen, edgecolor, edgewidth, False)

    # draw top edges
    for tr in top_regs:
        draw_lines(ax, tr, maxlen, edgecolor, edgewidth, True)

    # draw top projections
    for lin in top_projections:
        draw_projections(ax, lin, maxlen, 'b', 0.3, True)

    # draw bottom projections
    for lin in bottom_projections:
        draw_projections(ax, lin, maxlen, 'w', 0.3, False)

    # diagonal line (covers matrix aliasing)
    if diagonal is True:
        ax.add_line(Line2D([0, maxlen], [0, maxlen], color='grey', lw=1, linestyle='--'))
        # ax.add_line(Line2D([0, maxlen], [0, maxlen], color='w', lw=8))

    # adjust colorbar
    cb = ax.collections[0].colorbar
    lbl = np.absolute(cb.get_ticks())
    cb.ax.set_xticklabels(np.round(lbl, 1))
    cb.ax.xaxis.tick_top()
    cb.ax.tick_params(axis='x', which='major', length=0, labelsize=tfs)

    # calculate top ticks and ticklabels
    top_ticks_numeric = np.unique(np.concatenate(top_regs, axis=None))
    top_ticks_literal = np.mean(top_regs, axis=1)
    top_ticks_lit_lbl = string.ascii_letters[:len(top_ticks_literal)]

    # calculate bottom ticks and ticklabels
    bottom_ticks_numeric = np.unique(np.concatenate(bottom_regs, axis=None))
    bottom_ticks_literal = np.mean(bottom_regs, axis=1)
    bottom_ticks_lit_lbl = list(map(int2roman, range(1, len(bottom_ticks_literal) + 1)))

    # draw bottom ticks and ticklabels
    ax.set_yticks(bottom_ticks_literal)
    ax.set_yticklabels(bottom_ticks_lit_lbl)
    ax.tick_params(axis='y', which='major', length=0, labelsize=tfs)
    ax.set_xticks(bottom_ticks_literal)
    ax.set_xticklabels(bottom_ticks_lit_lbl, rotation=0)
    ax.tick_params(axis='x', which='major', length=0, labelsize=tfs)

    # draw top ticks and ticklabels for Y-axis
    ax2 = ax.twinx()
    ax2.set_xlim(ax.get_xlim())
    ax2.set_ylim(ax.get_ylim())
    ax2.set_yticks(top_ticks_literal)
    ax2.set_yticklabels(top_ticks_lit_lbl)
    ax2.tick_params(axis='y', which='major', length=0, labelsize=tfs)
    make_patch_spines_invisible(ax2)

    # draw top ticks and ticklabels for X-axis
    ax3 = ax.twiny()
    ax3.set_ylim(ax2.get_ylim())
    ax3.set_xlim(ax2.get_xlim())
    ax3.set_xticks(top_ticks_literal)
    ax3.set_xticklabels(top_ticks_lit_lbl)
    ax3.tick_params(axis='x', which='major', length=0, labelsize=tfs)
    make_patch_spines_invisible(ax3)

    # draw secondary bottom ticks and ticklabels
    if secondaryticks_left is not None:
        draw_secondary_ticks_left(ax, secondaryticks_left, bottom_ticks_numeric, fontsize=tfs)

    # draw secondary top ticks and ticklabels
    if secondaryticks_top is not None:
        draw_secondary_ticks_top(ax, secondaryticks_top, top_ticks_numeric, fontsize=tfs)

    plt.savefig(outname, dpi=300)
    # return fig, gs
    plt.close()


if __name__ == '__main__':

    # example execution
    pdb = "matrix-repeats-exons-test"
    exons = np.array([(1, 15), (15, 40), (40, 60)])
    units = np.array([(1, 24), (24, 50), (50, 60)])
    m = max(units[-1][-1], exons[-1][-1])
    exon_mat = distance_matrix_from_regions(exons, m, np.array([[0.5 for e in exons] for e in exons]), (0.00, 1))
    unit_mat = distance_matrix_from_regions(units, m, np.array([[0.2 for u in units] for u in units]), (-1, 0.00))
    cmat = merge_matrices(exon_mat, unit_mat, m)
    draw_matrix(cmat, units, exons, rboxes=[exons], bboxes=[units], bboxes_labels=['Units'],
                rboxes_labels=['Exons'],
                cmap_name='PuOr', secondaryticks_top='boundaries',
                secondaryticks_left="residues", outname='output-test/'+pdb + '.png', clip=True)
