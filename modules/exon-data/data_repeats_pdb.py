#!/usr/bin/env python
import argparse
import os
from updatedata import getrepeatdata

# configuration
import json
with open("modules/config.json") as json_data_file:
    config = json.load(json_data_file)


def download_repeatsdb(pdb, folder=None, outfl=None, reload_db=False):

    # check if RepeatsDB data in backup, else download
    if reload_db or not os.listdir(config['paths']['absolute']+config['paths']['repeatsdb3-data']):
        getrepeatdata()
    # read entries file
    entries = json.loads(open(config['paths']['absolute']+config['paths']['repeatsdb3-data']+"entries.json").read())
    objectspdb = [e for e in entries if e['repeatsdb_id'] == pdb]
    regionsobj = {r:{'units':[], 'insertions':[]} for r in set([e['region_id'] for e in objectspdb])}
    for e in objectspdb:
        if e['type'] == 'unit':
            regionsobj[e['region_id']]['units'].append([e['start'], e['end']])
        elif e['type'] == 'insertion':
            regionsobj[e['region_id']]['insertions'].append([e['start'], e['end']])
        else:
            print('Error! Entries types different that units and insertions are not allowed')
    if outfl and folder and regionsobj:
        with open(folder+outfl+'.json', 'w+') as outfile:
            json.dump([regionsobj[r] for r in regionsobj], outfile)


if __name__ == "__main__":  # execute only if run as a script

    def arg_parser():  # Parser implementation
        parser = argparse.ArgumentParser(prog='data-repeats-uniprot-pdb.py',
                                         epilog="  Example 1: data_repeats_pdb.py 1gxrA",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('pdb', help='RepeatsDB identifier, es: 1gxrA')
        parser.add_argument('--outfl', help='Outputfile path', default=None)
        return parser.parse_args()


    # parse and config arguments
    args = arg_parser()

    download_repeatsdb(args.pdb, folder=args.wdir if args.wdir else config['paths']['absolute']+config['paths']['repeatsdb3-data'],
                       outfl=args.outfl if args.outfl else 'data-repeats-pdb-test')