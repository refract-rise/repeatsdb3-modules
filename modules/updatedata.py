import json
import requests
import argparse
from ftplib import FTP

import os

# configuration
import json
with open("modules/config.json") as json_data_file:
    config = json.load(json_data_file)


def getrepeatdata():

    def gettype(type):
        # download from ws, generates entries.json file
        allentries_url = "https://repeatsdb.org/api/search?query=region_units_num:2/9999&show="+type
        url_data = requests.get(allentries_url).content
        js_data = json.loads(url_data)
        with open(config['paths']['absolute']+config['paths']['repeatsdb3-data']+type+".json", "w+") as output_fl:
            json.dump(js_data, output_fl)

    # download entries collection
    gettype('entries')
    # download uniprot collection
    gettype('uniprots')


def getuniprotdata():

    query = 'reviewed:yes'
    sort = 'score'
    columns = 'id,entry name,reviewed,protein names,genes,organism,length'
    columns += ',comment(DISEASE),comment(BIOTECHNOLOGY),comment(PHARMACEUTICAL),comment(BIOTECHNOLOGY)'  # disease
    columns += ',comment(SUBUNIT),interactor,'  # interactions
    columns += ',3d'  # structure
    columns += ',go,comment(SUBCELLULAR LOCATION),comment(PTM),feature(MODIFIED RESIDUE)'  # function and modifications
    columns += ',feature(REPEAT),comment(DOMAIN)'  # repeat and domains
    allentries_url = 'https://www.uniprot.org/uniprot/?query={}&sort={}&columns={}&format=tab'.format(query, sort, columns)
    url_data = requests.get(allentries_url).text
    with open(config['paths']['absolute'] + config['paths']['uniprot-data'] + query.replace(':','_')+".tab", "w+") as output_fl:
        output_fl.write(url_data)


def getintactdata():

    ftp = FTP('ftp.ebi.ac.uk')
    ftp.login()
    # Get All Files
    ftp.cwd('pub/databases/intact/current/psimitab/')
    files = ftp.nlst()
    print(files)
    # Print out the files
    for file in files:
        # files and folder
        if file == 'README':
            ftp.retrbinary("RETR " + file, open(config['paths']['absolute'] + config['paths']['intact-data'] + file, 'w+').write)
    ftp.close()


def getmobidbdata():
    # only human data
    url = "https://mobidb.bio.unipd.it/api/download?ncbi_taxon_id=9606&projection=prediction-disorder-th_50,acc,derived-missing_residues-th_90,derived-observed-th_90&format=json"
    url_data = requests.get(url).text
    with open(config['paths']['absolute'] + config['paths']['mobidb-data'] + 'mobidb_human.json', 'w+') as output_fl:
        output_fl.write(url_data)


if __name__ == "__main__":  # execute only if run as a script

    # configure folder
    for k in config['paths']:
        if not k == 'asbolute' and not os.path.exists(config['paths']['absolute'] + config['paths'][k]):
            os.makedirs(config['paths']['absolute'] + config['paths'][k])

    def str2bool(v):
        if v.lower() in ('yes', 'true', 't', 'y', '1'):
            return True
        elif v.lower() in ('no', 'false', 'f', 'n', '0'):
            return False
        else:
            raise argparse.ArgumentTypeError('Boolean value expected.')

    def arg_parser():  # Parser implementation
        parser = argparse.ArgumentParser(prog='updatedata.py',
                                         epilog="  Example 1: updatedata.py --uniprot --repeatsdb",
                                         formatter_class=argparse.RawDescriptionHelpFormatter)
        parser.add_argument('--uniprot', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Update UniProt data")
        parser.add_argument('--repeatsdb', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Update RepeatsDB data")
        parser.add_argument('--intact', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Update IntAct data")
        parser.add_argument('--mobidb', type=str2bool, nargs='?',
                            const=True, default=False,
                            help="Update MobiDB data")
        return parser.parse_args()


    # parse and config arguments
    args = arg_parser()

    if args.uniprot:
        getuniprotdata()
    if args.repeatsdb:
        getrepeatdata()
    if args.intact:
        getintactdata()
    if args.mobidb:
        getmobidbdata()
