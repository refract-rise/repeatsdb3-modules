# RepeatsDB 3 annotation modules

"RepeatsDB 3 annotation modules" is a Python library for dealing with RepeatsDB3 data. 
It includes:
   - A module for the download/visualization of exon data over PDB repeat structures.
   - A module for the analysis of repeat proteins and diseases association.
   - A module for the definition of repeats through a series of dichotomies 
     described in the manuscript "What is a repeat?".

## Activate environment

Use the virtual environment manager [venv](https://docs.python.org/3.6/library/venv.html) 
to activate the virtual environment.

```bash
source .venv/bin/activate
```

## Usage

### Download/update data

Make sure that all data is present and udpated in the folder `data` by running 
the `modules/updatedata.py` script and choosing the options:
   - `--uniprot` to update UniProt data
   - `--repeatsdb` to update RepeatsDB data
   - `--intact` to update IntAct data
   -  `--mobidb` to update MobiDB data

The first two are needed to run the repeat/exon and the repeat definition modules, 
the third is needed in addition to run the repeat/disease module, the fourth in addition
to run the repeat definition module.

### Repeat/exon module

All scripts in the repeat/exon module, in `modules/exon-data` are meant to be used also 
independently and their usage can be queried with the option `-h` or `--help`.
The only eception is the script `modules/exon-data/main.py` that runs all the others on 
RepeatsDB dataset by:
   - Downloading repeat data from RepeatsDB
   - Downloading exon data from the EMBL-EBI Protein API
   - Downloads PDB/UniProt mapping from SIFTS
   - Draws the repeat/exon matrix for each PDB chain containing repeats according to RepeatsDB.
   
To launch it, type in your terminal the script path and a input UniProt accession.
```bash
python modules/exon-data/main.py P29373 --download true
```
You will need to set the `download` option to `True` if this is the first time you run this 
example, in order to make sure that all the necessary input files are downloaded. If the input 
files are already downloaded, you will not need the `download` option to be active. 

The repeat data used by this script is store in `data/repeatsdb3-data` or any other path that
you set in `config.json`, "repeatsdb3-data". This folder should contain the `entries.json` file,
i.e. the "entries" collection in RepeatsDB in json format. If you would like to update this 
data, you have two options: manually download and substitute the relative file or run the 
main script with the option `update`.
```bash
python modules/exon-data/main.py P29373 --update true --download true
```

### Repeat/diseases module

The scripts in this module process Uniprot data to extract correlations between repeats, 
disease association and number of interactors to test the hypothesis of repeat proteins being
central nodes in PPI networks and therefore more associated to diseases than the background. 
An additional aim is to localise the interactions and disease-associated mutations in the 
protein to describe their overlap with repeat regions. 
To launch this module, type in your terminal the script path.
```bash
python modules/disease-data/repeat_disease.py
```
and eventually choose one of the following:
   - `--report` to write a basic analysis report. This option runs by default if none of the 
   following is selected;
   - `--interactions` to run a Student's t Test testing the hypothesis that, based on the 
   number of interactions, the subset of repeat proteins belongs to the same distribution 
   with respect to the background SwissProt;
   - `--diseases` to run the same test on the number of proteins associated to a disease;
   - `--ptms` to run the same test on the number of proteins containing post-translational 
   modifications;
   - `--modifications` to run the same test on the number of proteins containing mutations;

In alternative or in addition, you can run 
```bash
python modules/disease-data/interactions_analysis.py --reducefile --report
```
to parse IntAct data and select the subset of proteins that feature information about the 
binding region of their interactions. 

### Repeat definition module

The script in this module process SwissProt data, RepeatsDB data and MobiDB data
to plot the repeat lengths in structured and unstructured proteins. It needs
data input from `modules/updatedata.py` with the options `--uniprot`, 
`--mobidb` and `--repeatsdb`.
After loading the environment and updating the data, run it through:
```bash
python modules/repeat-definition/what_is_a_repeat.py
```
Please not: by default, the output plots and datasets are loaded in the output folder 
defined in `config.json`.


## Contributing
Pull requests are welcome. For major changes, please open an issue first to 
discuss what you would like to change.

## License 
[CC-BY-4.0](https://creativecommons.org/licenses/by/4.0/)

You are free to:
- Share — copy and redistribute the material in any medium or format
- Adapt — remix, transform, and build upon the material
for any purpose, even commercially.

Under the following terms:
- Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
- No additional restrictions — You may not apply legal terms or technological measures that legally restrict others from doing anything the license permits.



